const filmContainer = document.getElementById('film-container') as HTMLElement;
const favoriteContainer = document.getElementById(
    'favorite-movies',
) as HTMLElement;
const cards = document.querySelectorAll('.card');
const btnWrapper = document.getElementById('button-wrapper') as HTMLElement;
const searchInput = document.getElementById('search') as HTMLElement;
const searchBtn = document.getElementById('submit') as HTMLElement;
const loadMoreBtn = document.getElementById('load-more') as HTMLElement;
const randomMovieName = document.getElementById(
    'random-movie-name',
) as HTMLElement;
const randomMovieDescription = document.getElementById(
    'random-movie-description',
) as HTMLElement;

export {
    filmContainer,
    favoriteContainer,
    cards,
    btnWrapper,
    searchInput,
    searchBtn,
    loadMoreBtn,
    randomMovieName,
    randomMovieDescription
};