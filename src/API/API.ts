const API_KEY = 'be5a92f69a6e8dba07516d17315532c4';

const baseUrl = 'https://api.themoviedb.org/3/';

export enum UrlPaths {
    Search,
    GetFilm,
    GetPopular,
    GetTopRated,
    GetUpcoming,
}

export const getUrlPath = (params: {
    id: number | string;
    request: UrlPaths;
    pageNumber: number;
    movieName: string;
}): string => {
    switch (params.request) {
        case UrlPaths.GetFilm:
            return `${baseUrl}movie/${params.id}?api_key=${API_KEY}`;
        case UrlPaths.Search:
            return `${baseUrl}search/movie?api_key=${API_KEY}&language=en-US&query=${params.movieName}&page=${params.pageNumber}`;
        case UrlPaths.GetPopular:
            return `${baseUrl}movie/popular?api_key=${API_KEY}&page=${params.pageNumber}`;
        case UrlPaths.GetTopRated:
            return `${baseUrl}movie/top_rated?api_key=${API_KEY}&page=${params.pageNumber}`;
        case UrlPaths.GetUpcoming:
            return `${baseUrl}movie/upcoming?api_key=${API_KEY}&page=${params.pageNumber}`;
    }
};
