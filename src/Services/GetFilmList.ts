import { UrlPaths } from '../API/API';
import { Movie } from '../Interfaces/Interfaces';

export async function GetFilmList(url: UrlPaths): Promise<void> {

    interface URL {
        url: string;
    }

    function api<T>(url: URL): Promise<T> {
        return fetch(url).then((response) => {
            if (!response.ok) {
                throw new Error(response.statusText);
            }
            return response.json();
        });
    }



    const apiResult = api<{ results: Movie }>(url)
        .then(({ results }) =>
            Object.values(results).map((movie) => ({
                id: movie.id,
                title: movie.title,
                poster: movie.poster_path,
                releaseDate: movie.release_date,
                overview: movie.overview,
            }))
        )
        .catch((error) => {
            console.error(error);
        });

    return apiResult;
}
