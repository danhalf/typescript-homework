import { cards, filmContainer } from '../Constants/Selectors';
import { createRandomMovieCard } from '../Components/RandomMovieCard/RandomMovieCard';
import { Categories, MovieData } from '../Interfaces/Interfaces';
import { createCards } from '../Components/Cards/Card';
import { Variables } from '../Constants/Variables';
import { GetFilmList } from './GetFilmList';
import { getUrlPath, UrlPaths } from '../API/API';

const getCategory = (categories: Categories) => {
    switch (categories.event) {
        case 'search':
            return GetFilmList(
                getUrlPath({
                    request: UrlPaths.Search,
                    pageNumber: categories.pageNumber,
                    movieName: categories.movieName,
                }),
            );
        case 'popular':
            return GetFilmList(
                getUrlPath({
                    request: UrlPaths.GetPopular,
                    pageNumber: categories.pageNumber,
                }),
            );
        case 'upcoming':
            return GetFilmList(
                getUrlPath({
                    request: UrlPaths.GetUpcoming,
                    pageNumber: categories.pageNumber,
                }),
            );
        case 'top_rated':
            return GetFilmList(
                getUrlPath({
                    request: UrlPaths.GetTopRated,
                    pageNumber: categories.pageNumber,
                }),
            );
    }
};

export const getFilms = async (
    event: any,
    getParams: {
        selectorId: string | null;
        pageNumber: number;
    },
) => {
    event ? (Variables.pageNumber = 1) : Variables.pageNumber;
    const buttonId: string = getParams?.selectorId || event.target.id;
    Variables.currentCategory = buttonId;
    const filmList: any = await getCategory({
        event: buttonId,
        pageNumber: Variables.pageNumber,
        movieName: Variables.currentSearchValue,
    });
    filmContainer.innerHTML = null;
    try {
        const randomFilm: number = ~~(Math.random() * filmList.length + 1);
        const { overview, title } = filmList[randomFilm];
        createRandomMovieCard(overview, title);
        filmList.forEach((film: MovieData, index: number) => {
            const { id, poster, overview, releaseDate } = film;
            const isFavorite = (): boolean => {
                const currentStorage = window.localStorage.getItem('favoriteMovies');
                return currentStorage && currentStorage.split(',').map(el => Number(el)).includes(id);
            };
            createCards(filmContainer, {
                id,
                card: cards[index],
                poster,
                overview,
                releaseDate,
                isFavorite: isFavorite(),
            });
        });
    } catch (error) {
        return error;
    }
};