import { getFavoriteMovies, toggleFavoriteMovie } from './Components/FavoriteMoviesCard/FavoriteMoviesCard';
import { btnWrapper, filmContainer, loadMoreBtn, searchBtn, searchInput } from './Constants/Selectors';
import { getFilms } from './Services/GetFilms';
import { Variables } from './Constants/Variables';


export async function render(): Promise<void> {

    const loadMoreMovies = () => {
        Variables.pageNumber++;
        getFilms(null, { selectorId: Variables.currentCategory, pageNumber: Variables.pageNumber });
    };

    window.addEventListener('DOMContentLoaded', () => {
            getFilms(null, { selectorId: Variables.currentCategory });
            getFavoriteMovies();
        },
    );
    btnWrapper.addEventListener('click', getFilms);
    loadMoreBtn.addEventListener('click', loadMoreMovies);
    searchBtn.addEventListener('click', async () => {
        Variables.currentSearchValue = searchInput.value;
        await getFilms(null, { selectorId: 'search' });
    });
    filmContainer.addEventListener('click', toggleFavoriteMovie);
}
