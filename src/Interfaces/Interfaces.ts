interface Categories {
    event: string;
    pageNumber?: number;
    movieName?: string;
}

interface MovieData {
    id: number | string;
    card: HTMLElement;
    overview: string;
    releaseDate: string | number;
    poster: string;
    isFavorite: boolean;
}

interface CardData {
    text?: HTMLElement | null;
    title?: HTMLElement | null;
    release?: HTMLElement | null;
    img?: string | number;
    poster?: string;
}

interface Movie {
    id: number | string;
    title: string;
    posterPath: string;
    releaseDate: string;
    overview: string;
}

export { Categories, MovieData, CardData, Movie };
