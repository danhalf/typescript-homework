import { CardData } from '../../Interfaces/Interfaces';
import { randomMovieDescription, randomMovieName } from '../../Constants/Selectors';

const createRandomMovieCard = (text: CardData, title: CardData) => {
    randomMovieName.textContent = title;
    randomMovieDescription.textContent = text;
};

export { createRandomMovieCard };