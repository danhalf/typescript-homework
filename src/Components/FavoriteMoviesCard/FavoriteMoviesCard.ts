import { GetFilmList } from '../../Services/GetFilmList';
import { getUrlPath, UrlPaths } from '../../API/API';
import { createFavoriteCards } from '../Cards/FavoriteCard';
import { favoriteContainer } from '../../Constants/Selectors';

const getFavoriteMovies = async () => {
    const currStorage = await window.localStorage.getItem('favoriteMovies').split(',');
    currStorage.forEach( async (movieId )=> {
        const favMovie = await GetFilmList(getUrlPath({request: UrlPaths.GetFilm, id: movieId}))
        // TODO
    })
}

const toggleFavoriteMovie = (event: HTMLElement) => {
    const target = event.target;
    if (target.id.includes('movie_id')) {
        const svg = target.closest('svg');
        const currentMovie: [string, string | number] = target.id.split`-`;
        const [, id] = currentMovie;
        const currentLocalStorage = window.localStorage.getItem('favoriteMovies');
        if (!currentLocalStorage) {
            svg.style.fill = 'red';
            return window.localStorage.setItem('favoriteMovies', id);
        }
        let storage = currentLocalStorage && currentLocalStorage.split(',');
        if (storage && storage.includes(id)) {
            svg.style.fill = 'transparent';
            storage = storage.filter(elem => elem !== id);
            return window.localStorage.setItem('favoriteMovies', storage);
        }
        if (storage) storage = [...storage, id];

        svg.style.fill = 'red';
        window.localStorage.setItem('favoriteMovies', storage);

    }
};

export { getFavoriteMovies, toggleFavoriteMovie };