import { MovieData } from '../../Interfaces/Interfaces';

export const createCards = (
    containerSelector: HTMLElement,
    moviesData: MovieData
) => {
    containerSelector.insertAdjacentHTML(
        'beforeend',
        `
         <div class="col-lg-3 col-md-4 col-12 p-2">
            <div class="card shadow-sm">
               <img
                  src="https://image.tmdb.org/t/p/original/${moviesData.poster}"
               />
               <svg
                  xmlns="http://www.w3.org/2000/svg"
                  stroke="red"
                  fill=${ moviesData.isFavorite ? "red" : "transparent" }
                  width="50"
                  height="50"
                  class="bi bi-heart-fill position-absolute p-2"
                  viewBox="0 -2 18 22"
               >
                  <path
                     fill-rule="evenodd"
                     id=movie_id-${moviesData.id}
                     d="M8 1.314C12.438-3.248 23.534 4.735 8 15-7.534 4.736 3.562-3.248 8 1.314z"
                  />
               </svg>
               <div class="card-body">
                  <p class="card-text truncate">
                     ${moviesData.overview}
                  </p>
                  <div
                     class="
                        d-flex
                        justify-content-between
                        align-items-center
                     "
                  >
                     <small class="text-muted">${moviesData.releaseDate}</small>
                  </div>
               </div>
            </div>
         </div>
      `
    );
};
